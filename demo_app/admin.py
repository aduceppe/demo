from django.contrib import admin

from demo_app.models import Animal, Species


admin.site.register(Species)
admin.site.register(Animal)
