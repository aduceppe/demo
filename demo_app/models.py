import datetime

from django.db import models
from django.utils import timezone


class Species(models.Model):
    type = models.CharField(max_length=50)
    sound = models.CharField(max_length=50)
    created_date = models.DateTimeField('created date', auto_now_add=True)
    updated_date = models.DateTimeField('updated date', auto_now_add=True)

    def __str__(self):
        return self.type


class Animal(models.Model):
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    created_date = models.DateTimeField('created date', auto_now_add=True)
    updated_date = models.DateTimeField('updated date', auto_now_add=True)

    def __str__(self):
        return self.species.type
