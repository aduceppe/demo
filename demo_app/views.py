from django.db.models import QuerySet
from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect

from demo_app.models import Animal, Species


def index(request):
    return render_and_reload(request)


def make_sound(request, species_id):
    if request.method == 'POST':
        if species_id:
            species = get_object_or_404(Species, id=int(species_id))
            animal = Animal(species=species)
            animal.save()

    return redirect('demo_app:index')


def render_and_reload(request):
    animal_query = Animal.objects.all().order_by('-created_date').query
    animals = QuerySet(query=animal_query, model=Animal).order_by('id')[:5]

    context = {
        'species_list': get_list_or_404(Species),
        'animal_list': animals if animals else None,
    }
    return render(request, 'demo_app/index.html', context)
